// console.log("Hello World")
//1st mini activity
let result = 8**3;
console.log(`The cube of 8 is: ${result}`);

//2nd mini activity
const address = ['258','Washington Ave NW','California','90011'];
const [houseNum,street,state,areaCode] = address;
console.log(`I live at ${houseNum} ${street}, ${state}, ${areaCode}.`)

// 3rd mini activity
const animal = {
	name: 'Lolong',
	species: 'Saltwater Crocodile',
	weight: '1075 kgs',
	measurement: '20ft 3in'
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)


//4th mini activity

let numbers = [1,2,3,4,5];

numbers.forEach((number)=>{
	console.log(number);
})
let result1 = numbers.reduce((x,y) => x+y);
console.log(result1);


//5th mini activity

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let myDog = new Dog('Chowking','0.5','Chow-chow');
console.log(myDog);

























