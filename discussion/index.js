// console.log("Hello World")

// ES6 Updates

//exponent operator
//old
console.log("Result of old:")
const oldNum = Math.pow(8,2);
console.log(oldNum);

//new
console.log("Result of ES6:");
const newNum = 8**2;
console.log(newNum);

//Template Literals
/*
	Allows us to write strings without using the concatenate operator (+)

*/

let studentName = 'Roland'

//Pre-Template Literal String
console.log("Hello " + studentName + "! Welcome to programming")

//Template literal string
console.log('Template literal:')
console.log(`Hello ${studentName}! Welcome to programming`);

//Multi-line template literal
const message = `
${studentName} attended a math competition.
He won it by solving the problem 8 ** 2 with the solution of ${newNum}.
`
console.log(message);

/*
	Template literals allow us to write strings with embedded Javascript expressions

*/

const interestRate = .1;
const principal = 1000;

console.log(`The interest rate on your savings account is: ${principal * interestRate}`)



//Array Destructuring
/*
	Allows us to unpack elements in arrays into distinct variables
	Allows us to name array elements with variables instead of using index numbers
	Syntax:
		let/const [variableName, variableName, variableName] = array;
*/
console.log("Array Destructuring:")

const fullName = ['Jeru','Nebur','Palma'];

//Pre-Array Destructuring
console.log('Pre-array Destructuring')
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}`)

//Array Destructuring

const [firstName, middleName, lastName] = fullName;

console.log('Array Destructuring')
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)





// Object Destructuring
/*
	-Allows us to unpack properties on objects into distince values
	-Shortens the syntax for accessing properties from objects
	Syntax:
		let/const { propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: 'Jane',
	maidenName: 'Dela',
	familyName: 'Cruz'
};

//Pre object destructuring
console.log(person.givenName)
console.log(person.maidenName)
console.log(person.familyName)

//Object Destructuring
const {givenName, maidenName, familyName} = person;
console.log("After Object destucturing:")
console.log(givenName);
console.log(maidenName);
console.log(familyName);

console.log(`Hello ${givenName} ${maidenName} ${familyName} `)

//Arrow Functions

/*
	Compact alternative syntax to traditional functions
	-useful for creating code snippets where creating functions will not be reused in any other portions of the code
	-adheres to the DRY principle (Dont Repeat Yourself) where there is no longer a need to create a new function
*/

const hello = () => {
	console.log("Good morning Batch 241")
};

hello();

/*
	Syntax: 
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log();
		}
*/

const printFullName = (firstN, middleN, lastN) => {
	console.log(`${firstN} ${middleN} ${lastN}`)
}

printFullName('John', 'D', 'Smith');
console.log("");

//Arrow functions with loops
const students = ['John', 'Jane', 'Smith']

students.forEach((student) => {
	console.log(`${student} is a student`);
})

//Implicit return statement
/*
	there are instances when you can omit the "return" statement
*/


const add = (x,y) =>{
	return x+y;
}
let total = add(1,2)
console.log(total);

const subtract = (x,y) => x-y
let difference = subtract(3,1);
console.log(difference);
console.log("")

//Default argument value
/*
	-Provides a default argument if none is provided when the function is invoked
*/

console.log(`Default argument value:`)
const greet = (name = 'User') => `Good morning, ${name}`
console.log(greet());


// CLASS-BASED OBJECT BLUEPRINTS
//Allows creation / instantiation of objects as blueprints

//Creating a class
/*
	-The constructor function is a special method of class for creating / itnitializing an object for that class.
	-The 'this' keyword refers to the properties intitialized from the inside of the class
	Syntax:
		class className{
			constructor(objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyB = objectPropertyB;
			}
		}
*/

class Car{
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

/*
	-The "new" operator creates / instantiates a new object with the given arguments as the value
	-No arguments provided will create an object without any values assigned to its properties
	Syntax:
		let/const variableName = new className()
*/
console.log("")
console.log("Class Blueprints")
let myCar = new Car();
console.log(myCar);
console.log("");

/*
console.log("Console:")
console.log(console);
console.log(window);*/


//Assigning properties after creation / instantiation of an object
myCar.brand = 'Ford';
myCar.name = 'Ranger Raptor';
myCar.year = 2021;
console.log(myCar);


//Initiate a new object
const myNewCar = new Car ("Toyota","Vios",2021);
console.log(myNewCar);





























